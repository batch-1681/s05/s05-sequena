<?php session_start();

if (isset($_POST['logout'])) {
    unset($_SESSION['user']);
}
if (!isset($_SESSION['user'])){
    header("Location: index.php");
    exit();
}
?>
  <link rel="stylesheet" href="../d2/style.css">
<div class = "server">
<?php if (isset($_SESSION['user'])) : ?>
    <p>Hello, <?= $_SESSION['user'] ?></p>
<?php endif; ?>

<form method="post">
    <input type="hidden" name="logout">
    <input type="submit" value="logout" class="log">
</form>
</div>